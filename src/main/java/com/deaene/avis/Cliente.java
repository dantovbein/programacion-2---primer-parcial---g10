package com.deaene.avis;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Cliente extends Usuario implements Serializable {

    private ArrayList<Reserva> reservas;
    private String nombreCompleto;
    private Integer dni;
    private String telefono;
    private String direccion;
    
    public Cliente(String nombreUsuario, String clave, Integer id, String nombreCompleto, Integer dni, String telefono, String direccion) {
        super(nombreUsuario, clave, id, '3');
        this.dni = dni;
        this.nombreCompleto = nombreCompleto;
        this.telefono = telefono;
        this.direccion = direccion;
        this.reservas = new ArrayList<>();
    }
    
    public Cliente(String nombre, String clave, Integer id) {
        super(nombre, clave, id, '3');
    }
    
    @Override
    public boolean proceder(Sistema s) {
        this.setSistema(s);
        char opc;
        boolean seguir = true;
        do {
            opc = IO.leerChar(
                    "OPCIONES DEL CLIENTE\n"
                    + "[1] Mostrar datos del cliente \n"
                    + "[2] Mostrar mis reservas \n"     
                    + "[6] Desloguearse\n"
                    + "[7] Salir del sistema");
            switch (opc) {
                case '1':
                    this.mostrarDatos();
                    break;
                case '2':
                    this.mostrarReservas();
                    break;
                case '6':
                    seguir = true;
                    break;
                case '7':
                    seguir = false;
                    break;
                default:
                    IO.mostrarMensaje("ERROR: Opcion invalida");
                    opc = '*';
            }
            
            if (opc >= '1' && opc <= '2') {
                try {
                    this.getSistema().serializar("agencia.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
        } while (opc != '6' && opc != '7');

        return seguir;
    }
    
    public void agregarReserva(Reserva reserva) {
        this.reservas.add(reserva);
    }
    
    public void removerReserva(Reserva reserva) {
        this.reservas.remove(reserva);
    }
    
    public void mostrarReservas() {
        IO.mostrarMensaje("\n====== Reservas del cliente " + this.getNombreCompleto() + " =======\n");
        for(Reserva reserva : this.reservas) {
            reserva.mostrarReserva();
        }
    }
    
    @Override
    public void mostrarDatos() {
        IO.mostrarMensaje("--------------------------------------\n"
                           + "ID: " + this.getId()+ "\n"
                           + "Usuario: " + this.getNombreUsuario()+ "\n"
                           + "Tipo: " + this.getTipo()+ "\n"
                           + "Nombre completo: " + this.nombreCompleto + "\n"
                           + "DNI: " + this.dni + "\n"
                           + "Telefono: " + this.telefono + "\n"
                           + "Total de reservas: " + this.reservas.size());
    }
    
    public ArrayList<Reserva> getReservas() {
        return reservas;
    }

    public void setReservas(ArrayList<Reserva> reservas) {
        this.reservas = reservas;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public Integer getDni() {
        return dni;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }
}
