package com.deaene.avis;

import java.io.Serializable;

/**
 *
 * @author dan.tovbein
 */
public abstract class Usuario implements Serializable {
    private String nombreUsuario;
    private String clave;
    private Integer id;
    public char tipo;
    private Sistema sistema;
    
    public Usuario(String nombreUsuario, String clave, Integer id, char tipo) {
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
        this.id = id;
        this.tipo = tipo;
    }
    
    public abstract boolean proceder(Sistema sistema);
    
    public void mostrarDatos() {
        IO.mostrarMensaje("--------------------------------------\n"
                           + "ID: " + this.id + "\n"
                           + "Usuario: " + this.nombreUsuario + "\n"
                           + "Tipo: " + this.getTipo()+ "\n");
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getClave() {
        return clave;
    }

    public Integer getId() {
        return id;
    }

    public char getTipo() {
        return tipo;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    public Sistema getSistema() {
        return sistema;
    }
    
    
}
