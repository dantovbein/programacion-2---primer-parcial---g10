package com.deaene.avis;

import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Main {

    static Reserva reserva;
    static ArrayList<Reserva> reservas = new ArrayList<>();
    
    public static void main(String[] args) {
        Controlador controlador = new Controlador();
        try {
            controlador.iniciar();
        } catch (NullPointerException e) {
            IO.mostrarMensaje(e.getMessage());
        }
    }
}
