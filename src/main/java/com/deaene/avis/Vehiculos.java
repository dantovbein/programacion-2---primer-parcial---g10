/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deaene.avis;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Vehiculos implements Serializable {
    private static Vehiculos instance = new Vehiculos();
    private ArrayList<Vehiculo> vehiculos = new ArrayList<>();
    
    Vehiculos(){}
    
    static {
        try{
            instance = new Vehiculos();
        }catch(Exception e){
            throw new RuntimeException("Error al crear la instancia de Vehiculos");
        }
    }
    
    public void cargarVehiculo() {
        Integer ultimoId = this.vehiculos.size() == 0 ? 1 : this.vehiculos.get(this.vehiculos.size() - 1).getId();
        Integer id = ultimoId + 1;
        String marca = IO.leerString("Marca: ");
        if (marca.equals("")) {
            throw new NullPointerException("ERROR: La marca no puede ser nula.");
        }
        
        String modelo = IO.leerString("Modelo: ");
        if (modelo.equals("")) {
            throw new NullPointerException("ERROR: El modelo no puede ser nulo.");
        }
        
        String patente = IO.leerString("Patente: ");
        if (patente.equals("")) {
            throw new NullPointerException("ERROR: La patente no puede ser nula.");
        }
        
        String color = IO.leerString("Color: ");
        if (color.equals("")) {
            throw new NullPointerException("ERROR: El color no puede ser nulo.");
        }
        
        Integer importe = IO.leerNumero("Importe diario: ");
        if (importe.equals(null)) {
            throw new NullPointerException("ERROR: El importe no puede ser nulo.");
        }
        
        Integer litros = IO.leerNumero("Litros: ");
        if (litros.equals(null)) {
            throw new NullPointerException("ERROR: El valor de litros no puede ser nulo.");
        }
        
        agregarVehiculo(id, marca, modelo, patente, color, importe, litros);
    }
    
    public void agregarVehiculo(Integer id, String marca, String modelo, String patente, String color, Integer importe, Integer litros) {
        Vehiculo vehiculo = new Vehiculo(id, marca, modelo, patente, color, importe, litros);
        this.vehiculos.add(vehiculo);
        IO.mostrarMensaje("Nuevo vehiculo creado");
    }
    
    public void borrarVehiculo() {
        Integer id = IO.leerNumero("Ingrese el ID: ");
        for(Integer i = 0; i < this.vehiculos.size(); i++) {
            if(id.equals(this.vehiculos.get(i).getId())) {
                IO.mostrarMensaje("Borrando '" + this.vehiculos.get(i).getMarca() + " " + this.vehiculos.get(i).getModelo() + " ' ...");
                this.vehiculos.remove(this.vehiculos.get(i));
            }
        }
        IO.mostrarMensaje("Total de vehiculos: " + this.vehiculos.size());
    }
    
    public Vehiculo buscarVehiculoPorId(Integer id) {
        Usuario usuarioTemp;
        for (Vehiculo vehiculo : this.vehiculos) {
            if(vehiculo.getId().equals(id)) {
                return vehiculo;
            }
        }
        return null;
    }

    public void mostrarVehiculos() {
        IO.mostrarMensaje("\n\t\tVEHICULOS DISPONIBLES\nID\tEstado\t\tMarca\tModelo\tPatente\tColor\timporte diario\tTotal litros\tTotal litros disponibles");
        for (Vehiculo vehiculo : this.vehiculos) {
            IO.mostrarMensaje("[" + vehiculo.getId() + "]\t"
                               + (vehiculo.getDisponible() ? "Disponibe" : "N/A") + "\t"
                               + vehiculo.getMarca() + "\t"
                               + vehiculo.getModelo() + "\t"
                               + vehiculo.getPatente() + "\t"
                               + vehiculo.getPatente() + "\t"
                               + vehiculo.getColor() + "\t"
                               + vehiculo.getImporteDiario()+ "\t"
                               + vehiculo.getLitros()+ "\t"
                               + vehiculo.getLitrosDisponibles());
        }
    }
    
    public static Vehiculos getInstance(){
        return instance;
    }
}
