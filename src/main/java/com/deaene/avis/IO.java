package com.deaene.avis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.Timer;

/**
 *
 * @author dan.tovbein
 */
public class IO {
    public static String leerMensaje(String mensaje) {
        mensaje = mensaje == null ? "Mensaje no definido" : mensaje;
        String valor = JOptionPane.showInputDialog(mensaje);
        return valor;
    }
    
    public static boolean leerBooleano(String mensaje) {
        int i = JOptionPane.showConfirmDialog(null, mensaje, "Consulta", JOptionPane.YES_NO_OPTION);
        return i == JOptionPane.YES_OPTION;
    }
    
    public static String leerString(String texto) {
        String str = JOptionPane.showInputDialog(texto);
        return (str == null ? "" : str);
    }
    
    public static char leerChar(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        return (st == null || st.length() == 0 ? '0' : st.charAt(0));
    }
    
    public static Integer leerNumero(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        if(st == null) leerNumero(texto);
        Integer num = Integer.parseInt(st);
        return num;
    }
    
    public static Double leerDouble(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        if(st == null) leerNumero(texto);
        Double num = Double.parseDouble(st);
        return num;
    }
    
    public static void mostrarMensaje(String mensaje) {
        System.out.println(mensaje);
    }
    
    public static String leerPassword(String texto) {
        final JPasswordField pwd = new JPasswordField();
        ActionListener al = new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                pwd.requestFocusInWindow();
            }
        };
        Timer timer = new Timer(200, al);
        timer.setRepeats(false);
        timer.start();
        Object[] objs = {texto, pwd};
        String password = "";
        if (JOptionPane.showConfirmDialog(null, objs, "Entrada",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION) {
            password = String.valueOf(pwd.getPassword());
        }
        return password;
    }
}
