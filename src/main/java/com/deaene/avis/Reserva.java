package com.deaene.avis;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author dan.tovbein
 */
public class Reserva implements Serializable {
    
    private Integer id;
    private ArrayList<Vehiculo> vehiculos;
    private Oficina oficina;
    private Date fechaEntrega;
    private Date fechaDevolucion;
    private Integer dias;
    private long litros;
    private long importeTotal;
    private Sistema sistema;
    private Usuario usuario;
    private Boolean estado = true;
    private String tipo;
    
    
    public Reserva(Integer id, Sistema s) {
        this.sistema = s;
        this.id = id;
        this.vehiculos = new ArrayList<Vehiculo>();
        this.fechaEntrega = new Date();
        this.fechaDevolucion = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        dateFormat.format(this.fechaEntrega);
        dateFormat.format(this.fechaDevolucion);
        this.dias = IO.leerNumero("Ingrese el total de dias: ");
    }
    
    public void agregarOficina() {
        this.sistema.getOficinas().mostrarOficinas();
        Integer idOficina = IO.leerNumero("Ingrese el ID de la oficina");
        try {
            this.oficina = this.sistema.getOficinas().buscarOficinaPorId(idOficina);
        } catch(Exception e) {
            IO.mostrarMensaje("La oficina no existe\nIntente nuevamente");
            agregarOficina();
        }
        
        char tipo;
        do {
            tipo = IO.leerChar("Tipo de reserva:\n"
                + "[1] Presencial\n"
                + "[2] Telefonica");
        } while(tipo < '1' && tipo > '2');
        
        switch(tipo) {
            case '1':
                this.tipo = "Prrsencial";
                break;
            case '2':
                this.tipo = "Telefonica";
                break;
        }
    }
    
    public void agregarCliente() {
        IO.mostrarMensaje("**** Clientes *****");
        this.sistema.getUsuarios().mostrarUsuariosPorTipo('3');
        Integer idUsuario = IO.leerNumero("Ingrese el ID del usuario");
        this.usuario = this.sistema.getUsuarios().buscarUsuarioPorId(idUsuario);
        
        if(this.usuario instanceof Cliente) {
            Cliente tempCliente = (Cliente)this.usuario;
            tempCliente.agregarReserva(this);
        } else {
            IO.mostrarMensaje("El cliente no existe. Vuelva a agregar");
            agregarCliente();
        }
        
        IO.mostrarMensaje("Agregando " + this.usuario.getNombreUsuario() + "...");
    }
    
    public void agregarVehiculo() {
        Boolean agregar = false;
        Vehiculo vehiculo = null;
        Integer idVehiculo;
        this.sistema.getVehiculos().mostrarVehiculos();
        do {
            if(this.vehiculos.isEmpty()) {
                agregar = IO.leerBooleano("\nLa reserva debe tener al menos un vehiclo agregado.\n Desea agregar?");
            } else {
                this.sistema.getVehiculos().mostrarVehiculos();
                agregar = IO.leerBooleano("\n Desea agregar otro vehículo?");
            }
            
            if(agregar) {
                idVehiculo = IO.leerNumero("Ingrese el ID del vehiculo");
                vehiculo = this.sistema.getVehiculos().buscarVehiculoPorId(idVehiculo);
                
                if(!vehiculo.getDisponible()) {
                    IO.mostrarMensaje("El vehiculo " + vehiculo.getMarca() + " " + vehiculo.getModelo() + " no está disponible por el momento.");
                    agregarVehiculo();
                }
                
                agregar = IO.leerBooleano("El alquiler diario es de $" + Integer.toString(vehiculo.getImporteDiario()) + 
                                          "\nEl total por " + Integer.toString(this.dias)+ " es de $" + (Integer.toString(this.dias * vehiculo.getImporteDiario()))
                                          + "\nDesea agregarlo de todas formas?");
                if(agregar) {
                    vehiculo.setDisponible(false);
                    this.vehiculos.add(vehiculo);
                    this.importeTotal += this.dias * vehiculo.getImporteDiario();
                    IO.mostrarMensaje("vehiculo encontrado " + vehiculo.getMarca() + " " + vehiculo.getModelo());
                }
            }
        } while (agregar);
        
        if(this.vehiculos.isEmpty()) agregarVehiculo();
        IO.mostrarMensaje("Hay " + this.vehiculos.size() + " vehiculo agregados a la reserva");
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }
    
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<Vehiculo> getVehiculos() {
        return vehiculos;
    }
    
    public void mostrarReserva() {
        IO.mostrarMensaje("ID: " + this.id + "\n"
                           + "Oficina: " + this.oficina.getNombre() + "\n"
                           + "Tipo: " + this.tipo + "\n"
                           + "Fecha de entrega: " + this.fechaEntrega + "\n"
                           + "Total de dias: " + this.dias + "\n"
                           + "Fecha de devolución: " + this.fechaDevolucion + "\n"
                           + "Datos del cliente: " + this.usuario.getNombreUsuario() + "\n"
                           + "Cantidad de vehiculos: " + this.vehiculos.size());
        
        for(Vehiculo vehiculo : this.vehiculos) {
            IO.mostrarMensaje("\t" + vehiculo.getMarca() + " " + vehiculo.getModelo());
        }
        
        IO.mostrarMensaje("Importe total\n"  + "$" + this.importeTotal
                           + "\n-------------------------------------------------\n");
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
    
    public void finalizarReserva() {
        Boolean finalizar = IO.leerBooleano("Desea finalizar esta reserva?");
        if(finalizar) {
            for(Vehiculo vehiculo : this.vehiculos) {
                vehiculo.setDisponible(true);
            }
        }
    }
}
