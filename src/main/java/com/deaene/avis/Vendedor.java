package com.deaene.avis;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Vendedor extends Usuario implements Serializable {
    
    public Vendedor(String nombre, String clave, Integer id) {
        super(nombre, clave, id, '2');
    }
    
    @Override
    public boolean proceder(Sistema s) {
        this.setSistema(s);
        char opc;
        boolean seguir = true;
        do {
            opc = IO.leerChar(
                    "OPCIONES DEL VENDEDOR\n"
                    + "[1] Mostrar datos del vendedor\n"
                    + "[2] Cargar nuevo cliente\n"
                    + "[3] Mostrar clientes\n"
                    + "[4] Cargar nueva reserva\n"
                    + "[5] Mostrar reservas\n"
                    + "[6] Finalizar reserva\n"
                    + "[7] Desloguearse\n"
                    + "[8] Salir del sistema");
            switch (opc) {
                case '1':
                    this.mostrarDatos();
                    break;
                case '2':
                    this.getSistema().getUsuarios().crearUsuario('3');
                    break;
                case '3':
                    this.getSistema().getUsuarios().mostrarUsuariosPorTipo('3');
                    break;
                case '4':
                    this.getSistema().getReservas().cargarReserva(this.getSistema());
                    break;
                case '5':
                    this.getSistema().getReservas().mostrarReservas();
                    break;
                case '6':
                    this.getSistema().getReservas().finalizarReserva();
                    break;
                case '7':
                    seguir = true;
                    break;
                case '8':
                    seguir = false;
                    break;
                default:
                    IO.mostrarMensaje("ERROR: Opcion invalida");
                    opc = '*';
            }
            
            if (opc >= '1' && opc <= '6') {
                try {
                    this.getSistema().serializar("agencia.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
        } while (opc != '7' && opc != '8');

        return seguir;
    }
}
