package com.deaene.avis;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Administrador extends Usuario implements Serializable {
    
    public Administrador(String nombre, String clave, Integer id) {
        super(nombre, clave, id, '1');
    }
    
    @Override
    public boolean proceder(Sistema s) {
        this.setSistema(s);
        char opc;
        boolean seguir = true;
        do {
            opc = IO.leerChar(
                    "OPCIONES DEL ADMINISTRADOR\n"
                    + "[0] Agregar carga dinamica \n"
                    + "[1] Mostrar datos del administrador \n"
                    + "[2] Cargar usuario\n"
                    + "[3] Borrar usuario \n"
                    + "[4] Mostrar usuarios \n"
                    + "[5] Cargar vehiculo\n"
                    + "[6] Borrar vehiculo \n"
                    + "[7] Mostrar vehiculos \n"
                    + "[8] Oficinas \n"
                    + "[9] Desloguearse\n"
                    + "[q] Salir del sistema");
            switch (opc) {
                case '0':
                    // TODO Revisar OFICINAS
                    this.getSistema().getUsuarios().agregarUsuario('2', "ven1", "v");
                    this.getSistema().getUsuarios().agregarUsuario('2', "ven2", "v");
                    this.getSistema().getUsuarios().agregarCliente("cli1", "c", 4, "Dan Tovbein", 30191991, "36673476", "Av. Corrientes 1021");
                    this.getSistema().getUsuarios().agregarCliente("cli2", "c", 5, "Sebastian Bataglia", 10921821, "45838182", "Av. Rivadavia");
                    this.getSistema().getUsuarios().agregarCliente("cli3", "c", 6, "Roman Riquelme", 41020219, "47263911", "Cramer 192");
                    this.getSistema().getUsuarios().agregarCliente("cli4", "c", 7, "Carlos Tevez", 65029108, "28934176", "San Martin 10");
                    
                    this.getSistema().getVehiculos().agregarVehiculo(1, "Chevrolet", "Onix", "AAA911", "Gris", 100, 40);
                    this.getSistema().getVehiculos().agregarVehiculo(2, "Ford", "Escort", "TMD056", "Blanco", 75, 36);
                    this.getSistema().getVehiculos().agregarVehiculo(3, "Mercedes Benz", "CL200", "VIA911", "Rojo", 300, 45);
                    this.getSistema().getVehiculos().agregarVehiculo(4, "Volskwagen", "Astra", "AA 010 HI", "Negro", 100, 42);
                    this.getSistema().getVehiculos().agregarVehiculo(5, "Renault", "Clio", "ABC811", "Plata", 25, 42);
                    this.getSistema().getVehiculos().agregarVehiculo(6, "Ferrari", "F50", "6131HI", "Roja", 2530, 60);
                    this.getSistema().getVehiculos().agregarVehiculo(7, "Audi", "A4", "TJI1821", "Naranja", 1203, 28);
                    this.getSistema().getVehiculos().agregarVehiculo(8, "Ford", "Ranger", "UI13HI", "Dorado", 1000, 85);
                    this.getSistema().getVehiculos().agregarVehiculo(9, "Volskwagen", "Golf", "TT013", "Negro", 90, 23);
                    
                    this.getSistema().getOficinas().agregarOficina("Abasto");
                    this.getSistema().getOficinas().agregarOficina("Alto Palermo");
                    this.getSistema().getOficinas().agregarOficina("Paseo Alcorta");
                    this.getSistema().getOficinas().agregarOficina("DOT");
                    break;
                case '1': 
                    this.mostrarDatos();
                    break;
                case '2':
                    crearUsuario();
                    break;
                case '3':
                    this.getSistema().getUsuarios().borrarUsuario();
                    break;
                case '4':
                    this.getSistema().getUsuarios().mostrarUsuarios();
                    break;
                case '5':
                    this.getSistema().getVehiculos().cargarVehiculo();
                    break;
                case '6':
                    this.getSistema().getVehiculos().borrarVehiculo();
                    break;
                case '7':
                    this.getSistema().getVehiculos().mostrarVehiculos();
                    break;
                case '8':
                    seguir = this.getSistema().getOficinas().menu(this.getSistema());
                    break;
                case '9':
                    seguir = true;
                    break;
                case 'q':
                    seguir = false;
                    break;
                default:
                    IO.mostrarMensaje("ERROR: Opcion invalida");
                    opc = '*';
            }
            
            if (opc >= '0' && opc <= '8') {
                try {
                    this.getSistema().serializar("agencia.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
        } while (opc != '9' && opc != 'q');

        return seguir;
    }
    
    private void crearUsuario() {
        char tipo = IO.leerChar("SELECCIONA EL TIPO DE USUARIO: \n"
                                + "[1] Administrador\n"
                                + "[2] Vendedores\n"
                                + "[3] Clientes");
        
        this.getSistema().getUsuarios().crearUsuario(tipo);
    }
}
