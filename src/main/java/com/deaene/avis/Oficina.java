package com.deaene.avis;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Oficina implements Serializable {
    private Integer id;
    private String nombre;
    private ArrayList<Reserva> reservas;
    
    public Oficina(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.reservas = new ArrayList<Reserva>();
    }
    
    public void agregarReserva(Reserva reserva) {
        this.reservas.add(reserva);
    }
    
    public void mostrarDatos() {
        IO.mostrarMensaje("["+this.id+"]\t" + this.nombre);
    }

    public ArrayList<Reserva> getReservas() {
        return reservas;
    }

    public String getNombre() {
        return nombre;
    }
    
    
    
    public Integer getId() {
        return id;
    }
}
