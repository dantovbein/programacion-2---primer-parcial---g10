package com.deaene.avis;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Oficinas implements Serializable {
    private static Oficinas instance = new Oficinas();
    private ArrayList<Oficina> oficinas = new ArrayList<>();
    private Sistema sistema;
    
    Oficinas(){}
    
    static {
        try{
            instance = new Oficinas();
        }catch(Exception e){
            throw new RuntimeException("Error al crear la instancia de Oficinas");
        }
    }
    
    public boolean menu(Sistema s) {
        this.sistema = s;
        char opc;
        boolean seguir = true;
        do {
            opc = IO.leerChar(
                    "OPCIONES DEL ADMINISTRADOR\n"
                    + "[1] Agregar oficina \n"
                    + "[2] Borrar oficina \n"
                    + "[3] Mostrar oficinas \n"
                    + "[9] Salir del menu de oficinas\n");
            switch (opc) {
                case '1':
                    this.crearOficina();
                    break;
                case '2':
                    borrarOficina();
                    break;
                case '3':
                    this.mostrarOficinas();
                    break;
                case '9':
                    seguir = false;
                    break;
                default:
                    IO.mostrarMensaje("ERROR: Opcion invalida");
                    opc = '*';
            }
            
            if (opc >= '1' && opc <= '3') {
                try {
                    this.sistema.serializar("agencia.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                IO.mostrarMensaje("No entra");
            }
            
        } while (opc != '9');
        
        return seguir;
    }
    
    public void crearOficina() {
        String nombre = IO.leerString("Defina el nombre de la oficina");
        if(nombre == "") {
            IO.mostrarMensaje("Nombre incorrecto");
            crearOficina();
        }
        agregarOficina(nombre);
    }
    
    public void agregarOficina(String nombre) {
        Integer id = this.oficinas.isEmpty() ? 1 : this.oficinas.get(this.oficinas.size() - 1).getId() + 1;
        this.oficinas.add(new Oficina(id, nombre));
        IO.mostrarMensaje("Hay " + this.oficinas.size() + " oficinas");
    }
    
    public Oficina buscarOficinaPorId(Integer id) {
        Usuario usuarioTemp;
        for (Oficina oficina : this.oficinas) {
            if(oficina.getId().equals(id)) {
                return oficina;
            }
        }
        return null;
    }
    
    public void borrarOficina() {
        Integer id = IO.leerNumero("Ingrese el ID: ");
        for(Integer i = 0; i < this.oficinas.size(); i++) {
            if(id.equals(this.oficinas.get(i).getId())) {
                IO.mostrarMensaje("Borrando '" + this.oficinas.get(i).getNombre()+ "' ...");
                this.oficinas.remove(this.oficinas.get(i));
            }
        }
        IO.mostrarMensaje("Total de oficinas: " + this.oficinas.size());
    }
    
    public void mostrarOficinas() {
        IO.mostrarMensaje("\n------OFICINAS-------\nID\tNombre");
        
        for (Oficina oficina : this.oficinas) {
            oficina.mostrarDatos();
        }
    }

    public Sistema getSistema() {
        return sistema;
    }
    
    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

}
