package com.deaene.avis;

import java.io.IOException;

/**
 *
 * @author dan.tovbein
 */
public class Controlador {
    
    public void Controlador() {
        IO.mostrarMensaje("Ejecutando el controlador");
    }
    
    public void iniciar() {
        Sistema sistema = new Sistema();
        IO.mostrarMensaje("Iniciando el sistema");
        
        boolean seguir;
        try {
            sistema = sistema.deSerializar("agencia.txt");
            seguir = IO.leerBooleano("AGENCIA DE TURISMO AVIS\nDesea ingresar?");
        } catch (Exception e) {
            
            IO.mostrarMensaje("Arranque inicial del sistema");
            if(sistema.getUsuarios().getUsuarios().isEmpty()) {
                IO.mostrarMensaje("No hay usuarios registrados");
                sistema.getUsuarios().crearUsuario('1');
            }
            
            try {
                sistema.serializar("agencia.txt");
                IO.mostrarMensaje("El arranque ha sido exitoso. Ahora se debe reiniciar el sistema...");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            seguir = false;
        }
        
        while (seguir) {
            String nombre = IO.leerString("Ingrese el usuario:");
            String clave = IO.leerPassword("Ingrese la password:");
            
            Usuario usuario = sistema.getUsuarios().buscarUsuarioPorUsuarioClave(nombre + ":" + clave);

            if (usuario == null) {
                IO.mostrarMensaje("ERROR: La combinacion usuario/password ingresada no es valida.");
            } else {
                seguir = usuario.proceder(sistema);
            }
        }
    }
}
