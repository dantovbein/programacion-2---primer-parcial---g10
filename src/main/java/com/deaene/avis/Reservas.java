package com.deaene.avis;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Reservas implements Serializable {
    private static Reservas instance = new Reservas();
    private ArrayList<Reserva> reservas = new ArrayList<>();
    
    Reservas(){}
    
    static {
        try{
            instance = new Reservas();
        }catch(Exception e){
            throw new RuntimeException("Error al crear la instancia de Reservas");
        }
    }
    
    public void cargarReserva(Sistema sistema) {
        IO.mostrarMensaje("Cargando nueva reserva");
        Integer id = this.reservas.isEmpty() ? 1 : this.reservas.get(this.reservas.size() - 1).getId() + 1;
        Reserva reserva = new Reserva(id, sistema);
        
        reserva.agregarOficina();
        reserva.agregarCliente();
        reserva.agregarVehiculo();
        
        this.reservas.add(reserva);
        IO.mostrarMensaje("Nueva reserva creada");
        reserva.mostrarReserva();
    }
    
    public void mostrarReservas() {
        IO.mostrarMensaje("\n ============== Reservas =============\n"
                          + "Hay un total de " + this.reservas.size() + " reservas.\n");
        
        for (Reserva reserva : this.reservas) {
            reserva.mostrarReserva();
        }
    }
    
    public Reserva buscarReservaPorId(Integer id) {
        for (Reserva reserva : this.reservas) {
            if(reserva.getId().equals(id)) {
                return reserva;
            }
        }
        return null;
    }
    
    public void finalizarReserva() {
        IO.mostrarMensaje("\n ============== Reservas Activas =============\n"
                          + "Hay un total de " + this.reservas.size() + " reservas.\n");
        IO.mostrarMensaje("\nID\tUsuario\tTotal vehiculos\t\tFecha de entrega");
        for (Reserva reserva : this.reservas) {
            IO.mostrarMensaje("[" + reserva.getId() + "]\t" + reserva.getUsuario().getNombreUsuario() + "\t\t" + reserva.getVehiculos().size() + "\t" + reserva.getFechaEntrega());
        }
        
        Integer idReserva = IO.leerNumero("Ingrese la reserva que desea cancelar");
        Reserva reserva = buscarReservaPorId(idReserva);
        reserva.finalizarReserva();
        Cliente cliente = (Cliente)reserva.getUsuario();
        cliente.removerReserva(reserva);
        this.reservas.remove(reserva);
    }
}
