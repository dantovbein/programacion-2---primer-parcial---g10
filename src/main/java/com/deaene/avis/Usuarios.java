/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deaene.avis;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Usuarios implements Serializable {
    
    private static Usuarios instance = new Usuarios();
    private ArrayList<Usuario> usuarios = new ArrayList<>();
    
    Usuarios(){}
    
    static {
        try{
            instance = new Usuarios();
        }catch(Exception e){
            throw new RuntimeException("Error al crear la instancia de Usuarios");
        }
    }
    
    public void crearUsuario(char tipo) {
        IO.mostrarMensaje("Hay un total de " + this.usuarios.size() + " de usuarios");
        String nombre = IO.leerString("Ingrese el nombre: ");
        if (nombre.equals("")) {
            throw new NullPointerException("ERROR: El nombre no puede ser nulo.");
        }
        
        String clave = IO.leerPassword("Ingrese la clave");
        if (clave.equals("")) {
            throw new NullPointerException("ERROR: La clave no puede ser nula.");
        }
        
        IO.mostrarMensaje("Tipo: " + tipo);
        agregarUsuario(tipo, nombre, clave);
        
    }
    
    public void agregarUsuario(char tipo, String nombreUsuario, String clave) {
        IO.mostrarMensaje("Agregando nuevo usuario");
        Integer id = this.usuarios.isEmpty() ? 1 : this.usuarios.get(this.usuarios.size() - 1).getId() + 1;
        switch(tipo) {
            case '1':
                this.usuarios.add(new Administrador(nombreUsuario, clave, id));
                break;
            case '2':
                this.usuarios.add(new Vendedor(nombreUsuario, clave, id));
                break;
            case '3':
                String nombreCompleto = IO.leerString("Ingrese el nombre completo");
                if (nombreCompleto.equals("")) {
                    throw new NullPointerException("ERROR: El nombreCompleto no puede ser nulo.");
                }
                
                Integer dni = IO.leerNumero("Ingrese el DNI: (máximo 8 digitos)");
                if (dni.equals(null)) {
                    throw new NullPointerException("ERROR: El DNI no puede ser nulo.");
                }
                
                String telefono = IO.leerString("Ingrese el telefono");
                if (telefono.equals("")) {
                    throw new NullPointerException("ERROR: El telefono no puede ser nulo.");
                }
                
                String direccion = IO.leerString("Ingrese la direccion");
                if (direccion.equals("")) {
                    throw new NullPointerException("ERROR: La direccion no puede ser nula.");
                }
                
                this.usuarios.add(new Cliente(nombreUsuario, clave, id, nombreCompleto, dni, telefono, direccion));
                break;
        }
        IO.mostrarMensaje("Nuevo usuario creado");
    }
    
    // TODO borrar. Metodo utilizado solo para carga dinamica
    public void agregarCliente(String nombreUsuario, String clave, Integer id, String nombreCompleto, Integer dni, String telefono, String direccion) {
        this.usuarios.add(new Cliente(nombreUsuario, clave, id, nombreCompleto, dni, telefono, direccion));
        IO.mostrarMensaje("Nuevo cliente creado");
    }
    //
    
    public Usuario buscarUsuarioPorUsuarioClave(String datos) {
        IO.mostrarMensaje("Buscando usuario... " + datos);
        int i = 0;
        boolean encontrado = false;
        Usuario usr = null;

        while (i < this.usuarios.size() && !encontrado) {
            usr = this.usuarios.get(i);
            if (datos.equals(usr.getNombreUsuario()+ ":" + usr.getClave())) {
                encontrado = true;
            } else {
                i++;
            }
        }
        
        if (!encontrado) {
            IO.mostrarMensaje("Usuario '" + datos + "' no encontrado");
            return null;
        } else {
            IO.mostrarMensaje("Usuario '" + datos + "' encontrado!");
            return usr;
        }
    }
    
    public Usuario buscarUsuarioPorId(Integer id) {
        Usuario usuarioTemp;
        for (Usuario usuario : this.usuarios) {
            if(usuario.getId().equals(id)) {
                return usuario;
            }
        }
        return null;
    }
    
    public ArrayList<Usuario> obtenerUsuariosPorTipo(char tipo) {
        ArrayList<Usuario> usuariosFiltrados = new ArrayList<Usuario>();
        Usuario usuario;
        Character origTipo = new Character(tipo);
        Character tempTipo;
        for(Usuario u : this.usuarios) {
            tempTipo = new Character(u.getTipo());
            if(tempTipo.equals(tipo)) {
                usuariosFiltrados.add(u);
            }
        }
        return usuariosFiltrados;
    }
    
    public void mostrarUsuariosPorTipo(char tipo) {
        IO.mostrarMensaje("ID\tNombre");
        ArrayList<Usuario> usuariosFiltrados = obtenerUsuariosPorTipo(tipo);
        for(Usuario u : usuariosFiltrados) {
            IO.mostrarMensaje("[" + u.getId() + "]" + "\t" + u.getNombreUsuario());
        }
    }
    
    public void borrarUsuario() {
        Integer id = IO.leerNumero("Ingrese el ID: ");
        for(Integer i = 0; i < this.usuarios.size(); i++) {
            if(id.equals(this.usuarios.get(i).getId())) {
                IO.mostrarMensaje("Borrando '" + this.usuarios.get(i).getNombreUsuario() + "' ...");
                this.usuarios.remove(this.usuarios.get(i));
            }
        }
        IO.mostrarMensaje("Total de usuarios: " + this.usuarios.size());
    }
    
    // TODO mostrar solo clientes
    public void mostrarUsuarios() {
        IO.mostrarMensaje("\nID\tNombre\tClave\ttipo");
        for (Usuario usuario : this.getUsuarios()) {
            usuario.mostrarDatos();
        }
    }

    public static Usuarios getInstance(){
        return instance;
    }
    
    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }
}
