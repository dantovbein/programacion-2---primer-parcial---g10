package com.deaene.avis;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author dan.tovbein
 */
public class Sistema implements Serializable {
    
    private Usuarios usuarios;
    private Vehiculos vehiculos;
    private Reservas reservas;
    private Oficinas oficinas;
    
    public Sistema() {
        IO.mostrarMensaje("Sistema inicializado");
        usuarios = new Usuarios();
        vehiculos = new Vehiculos();
        reservas = new Reservas();
        oficinas = new Oficinas();
    }
    
    public Sistema deSerializar(String a) throws IOException, ClassNotFoundException {
        IO.mostrarMensaje("Deserializando...");
        FileInputStream f = new FileInputStream(a);
        ObjectInputStream o = new ObjectInputStream(f);
        Sistema s = (Sistema) o.readObject();
        o.close();
        f.close();
        return s;
    }

    public void serializar(String a) throws IOException {
        IO.mostrarMensaje("Serializando...");
        FileOutputStream f = new FileOutputStream(a);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
        f.close();
    }
    
    public Usuarios getUsuarios() {
        return usuarios;
    }
    
    public Vehiculos getVehiculos() {
        return vehiculos;
    }
    
    public Reservas getReservas() {
        return reservas;
    }
    
    public Oficinas getOficinas() {
        return oficinas;
    }
    
}
