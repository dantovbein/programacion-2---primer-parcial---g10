package com.deaene.avis;

import java.io.Serializable;

/**
 *
 * @author dan.tovbein
 */
public class Vehiculo implements Serializable {
    private Integer id;
    private String patente;
    private String modelo;
    private String color;
    private String marca;
    private Integer importeDiario;
    private Integer litros;
    private Integer litrosDisponibles;
    private Boolean disponible = true;
    
    public Vehiculo(Integer id, String marca, String modelo, String patente, String color, Integer importeDiario, Integer litros) {
        this.id = id;
        this.marca = marca;
        this.modelo = modelo;
        this.patente = patente;
        this.color = color;
        this.importeDiario = importeDiario;
        this.litros = litros;
        this.litrosDisponibles = litros;
    }

    public Integer getId() {
        return id;
    }

    public String getPatente() {
        return patente;
    }

    public String getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }

    public String getMarca() {
        return marca;
    }

    public Integer getImporteDiario() {
        return importeDiario;
    }

    public void setImporteDiario(Integer importe) {
        this.importeDiario = importe;
    }

    public Integer getLitros() {
        return litros;
    }
    
    public Integer getLitrosDisponibles() {
        return litrosDisponibles;
    }

    public void setLitrosDisponibles(Integer litrosDisponibles) {
        this.litrosDisponibles = litrosDisponibles;
    }

    public Boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(Boolean disponible) {
        this.disponible = disponible;
    }
    
    
}
